package com.paditech.rssreader.activities;

import java.util.List;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.paditech.rssreader.adapters.CategoryAdapter;
import com.paditech.rssreader.adapters.RSSFragmentPagerAdapter;
import com.paditech.rssreader.rss.Category;

import com.paditech.rssreader.R;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class RSSPagerActivity extends FragmentActivity{
	private SlidingMenu menu;
	private RSSFragmentPagerAdapter adapter;
	private ViewPager viewPager;
	private ProgressDialog dialog;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rss_pager_activity);
		setUpActionBar("Tin nóng");
		setUpDialog();
		setUpMenu();
		setUpPager();
	}
	
	@Override
	public void onBackPressed() {
		if (menu.isMenuShowing()) {
			menu.showContent(true);
			return;
		}
		if (isTaskRoot()) {
			new AlertDialog.Builder(this)
				.setIcon(R.drawable.ic_launcher)
				.setMessage(getString(R.string.exit_dialog))
				.setPositiveButton(getString(R.string.yes_button), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				})
				.setNegativeButton(getString(R.string.no_button), null)
				.show();
			return;
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.left_in, R.anim.right_out);	
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_action, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:            
				menu.toggle();
				return true; 
			case R.id.action_refresh:
				adapter.getRSSFragment(viewPager.getCurrentItem()).loadItem();
				return true;
			default:            
	        return super.onOptionsItemSelected(item);    
	   }
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			if (requestCode == 1) {
				viewPager.setCurrentItem(data.getIntExtra("menu", 0), true);
			}			
		} else {
			super.onActivityResult(requestCode, resultCode, data);	
		}
	}
	
	public void setActionBarTitle(String title) {
		View view = getActionBar().getCustomView();
		TextView txtTitle = (TextView) view.findViewById(R.id.title);
		txtTitle.setText(title);
	}
	
	private void setUpActionBar(String title) {
		ActionBar actionBar = getActionBar();
		View view = LayoutInflater.from(this).inflate(R.layout.actionbar, null);
		ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,ActionBar.LayoutParams.MATCH_PARENT, Gravity.CENTER);
		TextView txtTitle = (TextView) view.findViewById(R.id.title);
		txtTitle.setText(title);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
		actionBar.setCustomView(view,params);
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
	}
	
	private void setUpMenu() {
		// configure the SlidingMenu
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);
        ListView menuListView = (ListView) menu.findViewById(R.id.menu_list);
        View footer = LayoutInflater.from(this).inflate(R.layout.menu_item, null);
        TextView textView = (TextView) footer.findViewById(R.id.menu_title);
        ImageView imageView = (ImageView) footer.findViewById(R.id.menu_image);
        imageView.setImageResource(R.drawable.email);
        textView.setText(getResources().getString(R.string.contact_button_title));
        footer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("message/rfc822");
				intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.contact_email)});
				intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.contact_email_subject));
				startActivity(Intent.createChooser(intent, getResources().getString(R.string.contact_email_subject)));
			}
		});
        menuListView.addFooterView(footer);
        CategoryAdapter menuAdapter = CategoryAdapter.getInstance(RSSPagerActivity.this);
        menuListView.setAdapter(menuAdapter);
        menuListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				menu.toggle();
				viewPager.setCurrentItem(position, true);
			}
		});
	}
	
	private void setUpDialog(){
		dialog = new ProgressDialog(this);
		dialog.setMessage(getString(R.string.loading));
		dialog.setIndeterminate(false);
		dialog.setCancelable(false);
		dialog.show();
	}
	
	public ProgressDialog getDialog() {
		return dialog;
	}
	
	private void setUpPager() {
		viewPager = (ViewPager) findViewById(R.id.viewpager);
		List<Category> options = CategoryAdapter.getInstance(this).getMenuItems();
		adapter = new RSSFragmentPagerAdapter(getFragmentManager());
		adapter.setOptions(options);
		viewPager.setOffscreenPageLimit(CategoryAdapter.getInstance(this).getCount());
		viewPager.setAdapter(adapter);
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int position) {
				setActionBarTitle(adapter.getPageTitle(position).toString());
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {				
			}
		});
	}
}

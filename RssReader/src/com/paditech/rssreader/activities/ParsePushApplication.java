package com.paditech.rssreader.activities;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.PushService;
import com.parse.SaveCallback;

import android.app.Application;
import android.util.Log;

public class ParsePushApplication extends Application{
	
	private static String APP_ID = "mfmx3aMtlWyq4Kexm8WORVuvkXZIJdBGAF2smIBM";
	private static String CLIENT_KEY = "Q4sC19bEJgvvR6Aawr61l44hy6RPHsN6Fgjtc0Hi";
	
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate() {
		super.onCreate();
		
		// Initialize the Parse SDK.
		Parse.initialize(this, APP_ID, CLIENT_KEY); 
		
		ParsePush.subscribeInBackground("", new SaveCallback() {
			
			@Override
			public void done(ParseException ex) {
				if (ex == null) {
				      Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
				    } else {
				      Log.e("com.parse.push", "failed to subscribe for push", ex);
				    }
			}
		});
		
		// Specify an Activity to handle all pushes by default.
		PushService.setDefaultPushCallback(this, RSSListActivity.class);
	}
	
}

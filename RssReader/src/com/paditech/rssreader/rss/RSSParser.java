package com.paditech.rssreader.rss;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import java.net.HttpURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.paditech.rssreader.utilities.DateUtils;


public class RSSParser {
	private static String TAG_CHANNEL = "channel";
	private static String TAG_TITLE = "title";
	private static String TAG_DESCRIPTION = "description";
	private static String TAG_ITEM = "item";
	private static String TAG_DATE = "pubDate";
	private static String TAG_LINK = "link";
	private static String TAG_GUID = "guid";
	
	private static RSSParser instance;
	
	private RSSParser() {
	}
	
	public static RSSParser getInstance() {
		if (instance == null) {
			instance = new RSSParser();
		}
		return instance;
	}
	
	/**
     * Getting RSS feed items <item>
     * 
     * @param - rss link url of the website
     * @return - List of RSSItem class objects
     * */
	public List<RSSItem> getRSSFeedItemsByXML(String xml) {
		List<RSSItem> itemsList = new ArrayList<RSSItem>();
		String rss_feed_xml;
		rss_feed_xml = xml;
		if (rss_feed_xml != null) {
			try {
				Document doc = getDomElement(rss_feed_xml);
				NodeList nodeList = doc.getElementsByTagName(TAG_CHANNEL);
				Element channelElement = (Element) nodeList.item(0);
				
				NodeList items = channelElement.getElementsByTagName(TAG_ITEM);
				
				for (int i = 0; i < items.getLength(); i++) {
					Element itemElement = (Element) items.item(i);
					String title = this.getValue(itemElement, TAG_TITLE);
					String descriptionElement = this.getValue(itemElement, TAG_DESCRIPTION);
					org.jsoup.nodes.Document desDoc = Jsoup.parse(descriptionElement);
                    org.jsoup.nodes.Element img = desDoc.select("img").first();
                    org.jsoup.nodes.Element br = desDoc.select("br").first();
                    String image = "";
                    if (img != null) {
                    	image = img.attr("src");
                    }
                    String description = "";
                    if (br != null) {
	                    org.jsoup.nodes.Node node = br.nextSibling();
	                    description = node.toString();
                    }
					String date = this.getValue(itemElement, TAG_DATE);
					if (date != null) {
						date = DateUtils.dateConvert(date);
					}
					String link = this.getValue(itemElement, TAG_LINK);
					String guid = this.getValue(itemElement, TAG_GUID);
					if (title.length() != 0 && description.length() != 0 && date.length() != 0 && link.length() != 0) {
						RSSItem item = new RSSItem(title,description, date, link, guid, image);
						itemsList.add(item);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
		
		return itemsList;
	}
	
	/**
     * Getting RSS feed items <item>
     * 
     * @param - rss link url of the website
     * @return - List of RSSItem class objects
     * */
	public List<RSSItem> getRSSFeedItems(String rss_url) {
		List<RSSItem> itemsList = new ArrayList<RSSItem>();
		String rss_feed_xml;
		rss_feed_xml = getXmlfromUrl(rss_url);
		if (rss_feed_xml != null) {
			try {
				Document doc = getDomElement(rss_feed_xml);
				NodeList nodeList = doc.getElementsByTagName(TAG_CHANNEL);
				Element channelElement = (Element) nodeList.item(0);
				
				NodeList items = channelElement.getElementsByTagName(TAG_ITEM);
				
				for (int i = 0; i < items.getLength(); i++) {
					Element itemElement = (Element) items.item(i);
					String title = this.getValue(itemElement, TAG_TITLE);
					String descriptionElement = this.getValue(itemElement, TAG_DESCRIPTION);
					org.jsoup.nodes.Document desDoc = Jsoup.parse(descriptionElement);
                    org.jsoup.nodes.Element img = desDoc.select("img").first();
                    org.jsoup.nodes.Element br = desDoc.select("br").first();
                    String image = "";
                    if (img != null) {
                    	image = img.attr("src");
                    }
                    String description = "";
                    if (br != null) {
	                    org.jsoup.nodes.Node node = br.nextSibling();
	                    description = node.toString();
                    }
					String date = this.getValue(itemElement, TAG_DATE);
					if (date != null) {
						date = DateUtils.dateConvert(date);
					}
					String link = this.getValue(itemElement, TAG_LINK);
					String guid = this.getValue(itemElement, TAG_GUID);
					if (title.length() != 0 && description.length() != 0 && date.length() != 0 && link.length() != 0) {
						RSSItem item = new RSSItem(title,description, date, link, guid, image);
						itemsList.add(item);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
		
		return itemsList;
	}
	
	
	 /**
     * Getting RSS feed link from HTML source code
     * 
     * @param url is url of the website
     * @returns url of rss link of website
     * */
	public String getRSSLinkFromUrl(String url) {
		String rss_url = null;
		try {
			org.jsoup.nodes.Document doc = Jsoup.connect(url).get();
			Elements rss_links = doc.select("link[type=application/rss+xml]");
			if (rss_links.size() > 0) {
				rss_url = rss_links.get(0).attr("href").toString();
			} else {
				Elements atom_links = doc.select("link[type=application/atom+xml]");
				if (atom_links.size() > 0) {
					rss_url = atom_links.get(0).attr("href").toString();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rss_url;
	}
	
	/**
     * Method to get xml content from url HTTP Get request
     * */
	public String getXmlfromUrl(String url) {
		String xml = null;
		try {
			URL urlObj = new URL(url);
			HttpURLConnection httpConnection = (HttpURLConnection) urlObj.openConnection();
			httpConnection.setRequestMethod("GET");
			httpConnection.setRequestProperty("User-Agent", "Fiddler");  
			httpConnection.connect();
			if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				StringBuffer response = new StringBuffer();
				BufferedReader reader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
				String s = "";
				while ((s = reader.readLine()) != null) {
					response.append(s);
				}
				xml = response.toString();
			}
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return xml;
	}
	
	/**
	 * Getting XML DOM element
     * 
     * @param XML string
	 */
	public Document getDomElement(String xml) {
		Document doc = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));
			doc = (Document) db.parse(is);
		} catch (ParserConfigurationException e) {
			return null;
		} catch (SAXException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
		return doc;
	}
	
	/**
     * Getting node value
     * 
     * @param elem element
     */
    public final String getElementValue(Node elem) {
        Node child;
        if (elem != null) {
            if (elem.hasChildNodes()) {
                for (child = elem.getFirstChild(); child != null; child = child
                        .getNextSibling()) {
                    if (child.getNodeType() == Node.TEXT_NODE || ( child.getNodeType() == Node.CDATA_SECTION_NODE)) {
                        return child.getNodeValue();
                    }
                }
            }
        }
        return "";
    }
	
	/**
     * Getting node value
     * 
     * @param Element node
     * @param key  string
     * */
    public String getValue(Element item, String str) {
        NodeList n = item.getElementsByTagName(str);
        return this.getElementValue(n.item(0));
    }
}

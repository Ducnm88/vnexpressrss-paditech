package com.paditech.rssreader.rss;

public class Category {
	private String title;
	private String rss_url;
	private int image;
	
	public Category() {
		super();
	}
	
	public Category(String title, String url, int image) {
		super();
		this.title = title;
		this.rss_url = url;
		this.image = image;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getRss_url() {
		return rss_url;
	}
	
	public void setRss_url(String url) {
		this.rss_url = url;
	}
	
	public int getImage() {
		return image;
	}
	
	public void setImage(int image) {
		this.image = image;
	}
}

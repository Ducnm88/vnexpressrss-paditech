package com.paditech.rssreader.rss;

public class RSSItem {
	private String title;
	private String description;
	private String pubDate;
	private String link;
	private String guid;
	private String image;
	
	public RSSItem(String title, String description, String date, String link, String guid, String image) {
		this.title = title;
		this.description = description;
		this.pubDate = date;
		this.link = link;
		this.guid = guid;
		this.image = image;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getPubDate() {
		return pubDate;
	}
	
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	
	public String getLink() {
		return link;
	}
	
	public void setLink(String link) {
		this.link = link;
	}
	
	public String getGuid() {
		return guid;
	}
	
	public void setGuid(String guid) {
		this.guid = guid;
	}
	
	public String getImage() {
		return image;
	}
	
	public void setImage(String image) {
		this.image = image;
	}
}

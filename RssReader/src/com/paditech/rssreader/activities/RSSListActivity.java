package com.paditech.rssreader.activities;

import java.util.ArrayList;
import java.util.List;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.paditech.rssreader.adapters.CategoryAdapter;
import com.paditech.rssreader.adapters.RSSListAdapter;
import com.paditech.rssreader.rss.Category;
import com.paditech.rssreader.rss.RSSItem;
import com.paditech.rssreader.rss.RSSParser;
import com.paditech.rssreader.utilities.ConnectionDetector;

import com.paditech.rssreader.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class RSSListActivity extends Activity {
	
	private SlidingMenu menu;
	private ProgressDialog dialog;
	
	RSSParser rssParser = RSSParser.getInstance();
	
	private String rss_link;
	private String title;
	private RSSListAdapter adapter;
	private List<RSSItem> rssItems = new ArrayList<RSSItem>();
	
	// for swipe detection
	private float x1,x2;
	static final int MIN_DISTANCE = 150;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rss_list_activity);
		rss_link = "http://vnexpress.net/rss/tin-moi-nhat.rss";
		title = "Tin nóng";
		Intent intent = getIntent();
		String url = intent.getStringExtra("rss_url");
		String name = intent.getStringExtra("title");
		if (url != null && !url.equals(rss_link)) {
			rss_link = url;
			title = name;
		}
		setUpActionBar(title);
		setUpMenu();
		setUpListView();

		new LoadRSSItem().execute(rss_link);
	}
	
	@Override
	public void onBackPressed() {
		if (isTaskRoot()) {
			new AlertDialog.Builder(this)
				.setIcon(R.drawable.ic_launcher)
				.setMessage(getString(R.string.exit_dialog))
				.setPositiveButton(getString(R.string.yes_button), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				})
				.setNegativeButton(getString(R.string.no_button), null)
				.show();
			return;
		} else {
			super.onBackPressed();
			overridePendingTransition(R.anim.left_in, R.anim.right_out);	
		}
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev){
	    super.dispatchTouchEvent(ev);    
	    return this.onTouchEvent(ev); 
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch(event.getAction())
	    {
	      case MotionEvent.ACTION_DOWN:
	          x1 = event.getX();                         
	          break;         
	      case MotionEvent.ACTION_UP:
	          x2 = event.getX();
	          float deltaX = x2 - x1;
	          if (Math.abs(deltaX) > MIN_DISTANCE) {
	              // Left to Right swipe action
	              if (x2 > x1) {  
	              }
	              // Right to left swipe action               
	              else {
	            	  if (menu.isMenuShowing()) {
	  					menu.showContent(true);
	            	  }
	              }
	          }
	          else {
	              // consider as something else - a screen tap for example
	          }                          
	          break;   
	    }          
		return super.onTouchEvent(event);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_action, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:            
				menu.toggle();
				return true; 
			case R.id.action_refresh:
				new LoadRSSItem().execute(rss_link);
				return true;
			default:            
	         return super.onOptionsItemSelected(item);    
	   }
	}
	
	private void setUpActionBar(String title) {
		ActionBar actionBar = getActionBar();
		View view = LayoutInflater.from(this).inflate(R.layout.actionbar, null);
		ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,ActionBar.LayoutParams.MATCH_PARENT, Gravity.CENTER);
		TextView txtTitle = (TextView) view.findViewById(R.id.title);
		txtTitle.setText(title);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
		actionBar.setCustomView(view,params);
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
	}
	
	private void setUpMenu() {
		// configure the SlidingMenu
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);
        ListView menuListView = (ListView) menu.findViewById(R.id.menu_list);
        CategoryAdapter menuAdapter = CategoryAdapter.getInstance(RSSListActivity.this);
        menuListView.setAdapter(menuAdapter);
        menuListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				CategoryAdapter menuAdapter = (CategoryAdapter) parent.getAdapter();
				Category item = (Category) menuAdapter.getItem(position);
				Intent intent = new Intent(RSSListActivity.this, RSSListActivity.class);
				intent.putExtra("rss_url", item.getRss_url());
				intent.putExtra("title", item.getTitle());
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
	}
	
	private void setUpListView() {
		ListView listView = (ListView)findViewById(R.id.rss_list);
		adapter = new RSSListAdapter(RSSListActivity.this);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				Intent intent = new Intent(RSSListActivity.this, RSSItemDetailActivity.class);
				RSSListAdapter adapter = (RSSListAdapter) parent.getAdapter();
				RSSItem item = (RSSItem)adapter.getItem(position);
				intent.putExtra("url", item.getLink());
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
	}
	
	class LoadRSSItem extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(RSSListActivity.this);
			dialog.setMessage(getString(R.string.loading));
			dialog.setIndeterminate(false);
			dialog.setCancelable(false);
			dialog.show();
			if (ConnectionDetector.isNetworkAvailable(RSSListActivity.this)) {
			} else {
				Toast.makeText(RSSListActivity.this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
			}
		}
		
		@Override
		protected String doInBackground(String... params) {
			String rss_url = params[0];
			
			rssItems = rssParser.getRSSFeedItems(rss_url);
			
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			adapter.setRssItems(rssItems);
			dialog.dismiss();
		}
		
	}
}

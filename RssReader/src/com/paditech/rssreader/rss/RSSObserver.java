package com.paditech.rssreader.rss;

import java.util.List;

public interface RSSObserver {
	void update(List<RSSItem> items);
}

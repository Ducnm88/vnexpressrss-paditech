package com.paditech.rssreader.activities;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.paditech.rssreader.adapters.CategoryAdapter;
import com.paditech.rssreader.utilities.ConnectionDetector;

import com.paditech.rssreader.R;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class RSSItemDetailActivity extends Activity{
	
	private SlidingMenu menu;
	private WebView webView;
	private ProgressDialog dialog;
	private String url;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rss_detail_activity);
		Intent intent = getIntent();
		url = intent.getStringExtra("url");
		setUpMenu();
		setUpActionBar("");
		setUpWebview();
		dialog = new ProgressDialog(RSSItemDetailActivity.this);
		dialog.setMessage(getString(R.string.loading));
		dialog.setIndeterminate(false);
		dialog.show();
		if (ConnectionDetector.isNetworkAvailable(this)) {
			webView.loadUrl(url);	
		} else {
			Toast.makeText(RSSItemDetailActivity.this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.left_in, R.anim.right_out);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_action, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:            
				menu.toggle();
				return true; 
			case R.id.action_refresh:
				dialog.show();
				if (ConnectionDetector.isNetworkAvailable(this)) {
					webView.loadUrl(url);	
				} else {
					Toast.makeText(RSSItemDetailActivity.this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
				}
				return true;
			default:            
	        return super.onOptionsItemSelected(item);    
	   }
	}
	
	private void setUpActionBar(String title) {
		ActionBar actionBar = getActionBar();
		View view = LayoutInflater.from(this).inflate(R.layout.actionbar, null);
		ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,ActionBar.LayoutParams.MATCH_PARENT, Gravity.CENTER);
		TextView txtTitle = (TextView) view.findViewById(R.id.title);
		txtTitle.setText(title);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
		actionBar.setCustomView(view,params);
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
	}
	
	private void setUpMenu() {
		// configure the SlidingMenu
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);
        ListView menuListView = (ListView) menu.findViewById(R.id.menu_list);
        View footer = LayoutInflater.from(this).inflate(R.layout.menu_item, null);
        TextView textView = (TextView) footer.findViewById(R.id.menu_title);
        ImageView imageView = (ImageView) footer.findViewById(R.id.menu_image);
        imageView.setImageResource(R.drawable.email);
        textView.setText(getResources().getString(R.string.contact_button_title));
        footer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("message/rfc822");
				intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.contact_email)});
				intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.contact_email_subject));
				startActivity(Intent.createChooser(intent, getResources().getString(R.string.contact_email_subject)));
			}
		});
        menuListView.addFooterView(footer);
        CategoryAdapter menuAdapter = CategoryAdapter.getInstance(RSSItemDetailActivity.this);
        menuListView.setAdapter(menuAdapter);
        menuListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				menu.toggle();
				Intent data = new Intent();
				data.putExtra("menu", position);
				setResult(RESULT_OK, data);
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
	}
	
	@SuppressWarnings("deprecation")
	public void setUpWebview() {
		webView = (WebView) findViewById(R.id.webView);
		webView.getSettings().getJavaScriptEnabled();
	    webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
	    webView.getSettings().setPluginState(PluginState.ON);
	    webView.getSettings().setSupportZoom(true);
	    webView.getSettings().setBuiltInZoomControls(true);
	    webView.getSettings().setDisplayZoomControls(false);
	    webView.getSettings().setAllowFileAccess(true);
		webView.setWebViewClient(new RssItemDetailWebClient());
	}
	
	private class RssItemDetailWebClient extends WebViewClient {
		
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
		
		@Override
		public void onPageFinished(WebView view, String url) {
			if (dialog != null && dialog.isShowing()) {
				dialog.dismiss();
			}
		}
	}
}

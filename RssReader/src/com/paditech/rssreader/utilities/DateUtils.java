package com.paditech.rssreader.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
	
	public static String dateConvert(String inputDate){
		String dateString = "";
		SimpleDateFormat inputFormat = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z",Locale.US);
		Date date;
		try {
			date = inputFormat.parse(inputDate);
			dateString = dateDiff(date, new Date());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateString;
	}
	
	public static String dateDiff(Date from, Date to) {
		long diff = to.getTime() - from.getTime();
		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);
		if (diffDays > 0) {
			return diffDays + " ngày trước";
		}
		if (diffHours > 0) {
			return diffHours + " giờ trước";
		}
		if (diffMinutes > 0) {
			return diffMinutes + " phút trước";
		}
		if (diffSeconds > 0) {
			return "1 phút trước";
		}
		return "";
	}
}

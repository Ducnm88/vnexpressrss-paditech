package com.paditech.rssreader.rss;

import java.util.ArrayList;
import java.util.List;

import com.paditech.rssreader.utilities.ConnectionDetector;

import com.paditech.rssreader.R;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

public class RSSAsyncTask extends AsyncTask<String, String ,List<RSSItem>> {
	List<RSSObserver> observers = new ArrayList<RSSObserver>();
	ProgressDialog dialog;
	
	public RSSAsyncTask(ProgressDialog dialog) {
		this.dialog = dialog;
	}
	
	public void attachObserver(RSSObserver observer) {
		observers.add(observer);
	}
	
	public void detachObserver(RSSObserver observer) {
		observers.remove(observer);
	}
	
	private void notifyObserver(List<RSSItem> items) {
		for (RSSObserver observer : observers) {
			observer.update(items);
		}
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (!dialog.isShowing()) {
			dialog.show();	
		}
		if (ConnectionDetector.isNetworkAvailable(dialog.getContext())) {
			
		} else {
			Toast.makeText(dialog.getContext(), R.string.network_error, Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	protected List<RSSItem> doInBackground(String... params) {
		return RSSParser.getInstance().getRSSFeedItems(params[0]);
	}
	
	@Override
	protected void onPostExecute(List<RSSItem> result) {
		super.onPostExecute(result);
		if (result != null) {
			notifyObserver(result);
		}
		if (dialog.isShowing()) {
			dialog.dismiss();	
		}
	}
	
}

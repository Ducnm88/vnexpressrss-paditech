package com.paditech.rssreader.adapters;

import java.util.List;

import com.paditech.rssreader.rss.RSSItem;
import com.paditech.rssreader.rss.RSSObserver;
import com.squareup.picasso.Picasso;

import com.paditech.rssreader.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RSSListAdapter extends BaseAdapter implements RSSObserver{
	
	private List<RSSItem> rssItems;
	private Context context;
	
	public RSSListAdapter(Context context) {
		super();
		this.context = context;
	}
	
	public List<RSSItem> getRssItems() {
		return rssItems;
	}
	
	public void setRssItems(List<RSSItem> rssItems) {
		this.rssItems = rssItems;
		this.notifyDataSetChanged();
	}
	
	@Override
	public int getViewTypeCount() {
		return 2;
	}
	
	@Override
	public int getItemViewType(int position) {
		if (position == 0) {
			return 0;
		}
		return 1;
	}
	
	@Override
	public int getCount() {
		if (rssItems != null) {
			return rssItems.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if (rssItems != null) {
			return rssItems.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			if (getItemViewType(position) == 0) {
				convertView = LayoutInflater.from(context).inflate(R.layout.first_rss_item, null);
			} else {
				convertView = LayoutInflater.from(context).inflate(R.layout.rss_item, null);
			}
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		ImageView imageView = (ImageView)convertView.findViewById(R.id.image);
		TextView titleTextView = (TextView)convertView.findViewById(R.id.title);
		TextView dateTextView = (TextView)convertView.findViewById(R.id.pub_date);
		RSSItem item = rssItems.get(position);
		if (item.getImage().length() > 0) {
			Picasso.with(context).load(item.getImage()).placeholder(R.drawable.no_image).into(imageView);	
		} else {
			imageView.setImageResource(R.drawable.no_image);
		}
		titleTextView.setText(item.getTitle());
		dateTextView.setText(item.getPubDate());
		TextView descTextView = (TextView)convertView.findViewById(R.id.description);
		if (descTextView != null) {
			String desc = item.getDescription();
			if (desc.length() > 100) {
				desc = (desc.substring(0, 99).substring(0, desc.substring(0, 99).lastIndexOf(" "))) + "...";
			}
			descTextView.setText(desc);
		}
		return convertView;
	}
	
	@Override
	public void update(List<RSSItem> items) {
		setRssItems(items);
	}
	
	static class ViewHolder {
		LinearLayout layout;
		ImageView imageView;
		TextView textTitle;
		TextView textDate;
	}
}

package com.paditech.rssreader.adapters;

import java.util.ArrayList;
import java.util.List;

import com.paditech.rssreader.rss.Category;

import com.paditech.rssreader.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CategoryAdapter extends BaseAdapter{
	
	private Context context;
	private List<Category> categoryItems;
	
	public static CategoryAdapter instance;
	
	public static CategoryAdapter getInstance(Context context) {
		if (instance == null) {
			instance = new CategoryAdapter(context);
		}
		instance.context = context;
		return instance;
	}
	
	private CategoryAdapter(Context context) {
		super();
		this.context = context;
		setCategory();
	}
	
	private void setCategory() {
		categoryItems = new ArrayList<Category>();
		categoryItems.add(new Category(context.getResources().getString(R.string.home_category), context.getResources().getString(R.string.home_url),R.drawable.home));
		categoryItems.add(new Category(context.getResources().getString(R.string.news_category), context.getResources().getString(R.string.news_url),R.drawable.news));
		categoryItems.add(new Category(context.getResources().getString(R.string.life_category), context.getResources().getString(R.string.life_url),R.drawable.life));
		categoryItems.add(new Category(context.getResources().getString(R.string.world_category), context.getResources().getString(R.string.world_url),R.drawable.world));
		categoryItems.add(new Category(context.getResources().getString(R.string.business_category), context.getResources().getString(R.string.business_url),R.drawable.business));
		categoryItems.add(new Category(context.getResources().getString(R.string.entertainment_category), context.getResources().getString(R.string.entertainment_url),R.drawable.entertainment));
		categoryItems.add(new Category(context.getResources().getString(R.string.sport_category), context.getResources().getString(R.string.sport_url),R.drawable.sport));
		categoryItems.add(new Category(context.getResources().getString(R.string.law_category), context.getResources().getString(R.string.law_url),R.drawable.law));
		categoryItems.add(new Category(context.getResources().getString(R.string.travel_category), context.getResources().getString(R.string.travel_url),R.drawable.travelling));
		categoryItems.add(new Category(context.getResources().getString(R.string.science_category), context.getResources().getString(R.string.science_url),R.drawable.science));
		categoryItems.add(new Category(context.getResources().getString(R.string.digital_category), context.getResources().getString(R.string.digital_url),R.drawable.digital));
		categoryItems.add(new Category(context.getResources().getString(R.string.car_category), context.getResources().getString(R.string.car_url),R.drawable.car));
		categoryItems.add(new Category(context.getResources().getString(R.string.social_category), context.getResources().getString(R.string.social_url),R.drawable.social));
		categoryItems.add(new Category(context.getResources().getString(R.string.chat_category), context.getResources().getString(R.string.chat_url),R.drawable.chat));
		categoryItems.add(new Category(context.getResources().getString(R.string.funny_category), context.getResources().getString(R.string.funny_url),R.drawable.funny));
		this.notifyDataSetChanged();
	}
	
	public List<Category> getMenuItems() {
		return categoryItems;
	}
	
	@Override
	public int getCount() {
		return categoryItems.size();
	}

	@Override
	public Object getItem(int position) {
		return categoryItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder =  new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.menu_item, null);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder)convertView.getTag();
		}
		Category item = categoryItems.get(position);
		TextView textView = (TextView) convertView.findViewById(R.id.menu_title);
		ImageView imageView = (ImageView) convertView.findViewById(R.id.menu_image);
		textView.setText(item.getTitle());
		imageView.setImageResource(item.getImage());
		return convertView;
	}
	
	static class ViewHolder {
		TextView textView;
		ImageView imageView;
	}

}

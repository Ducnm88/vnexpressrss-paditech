package com.paditech.rssreader.adapters;

import java.util.HashMap;
import java.util.List;

import com.paditech.rssreader.fragments.RSSFragment;
import com.paditech.rssreader.rss.Category;


import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.view.View;

public class RSSFragmentPagerAdapter extends FragmentStatePagerAdapter {
	
	HashMap<Integer, RSSFragment> mPageReferenceMap = new HashMap<Integer, RSSFragment>();
	
	public RSSFragmentPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	List<Category> options;
	
	public void setOptions(List<Category> options) {
		this.options = options;
		notifyDataSetChanged();
	}
	
	@Override
	public Fragment getItem(int position) {
		RSSFragment fragment = RSSFragment.getInstance(options.get(position));
		mPageReferenceMap.put(position, fragment);
		return fragment;
	}

	@Override
	public int getCount() {
		return options.size();
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		return options.get(position).getTitle();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void destroyItem(View container, int position, Object object) {
		super.destroyItem(container, position, object);
		mPageReferenceMap.remove(position);
	}
	
	public RSSFragment getRSSFragment(int position) {
		return mPageReferenceMap.get(position);
	}
}

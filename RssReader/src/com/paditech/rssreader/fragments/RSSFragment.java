package com.paditech.rssreader.fragments;

import com.paditech.rssreader.activities.RSSItemDetailActivity;
import com.paditech.rssreader.activities.RSSPagerActivity;
import com.paditech.rssreader.adapters.RSSListAdapter;
import com.paditech.rssreader.rss.Category;
import com.paditech.rssreader.rss.RSSAsyncTask;
import com.paditech.rssreader.rss.RSSItem;

import com.paditech.rssreader.R;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class RSSFragment extends Fragment{
	
	
	private String rss_link;
	private RSSListAdapter adapter;
	private RSSAsyncTask task;
	private ProgressDialog dialog;
	
	public static RSSFragment getInstance(Category category) {
		RSSFragment fragment = new RSSFragment();
		Bundle bundle = new Bundle();
		bundle.putString("url", category.getRss_url());
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View root = (View) inflater.inflate(R.layout.rss_list_activity, null);
		rss_link = getArguments().getString("url");
		setUpListView(root);
		setUpDialog();
		loadItem();
		return root;
	}
	
	private void setUpListView(View view) {
		ListView listView = (ListView)view.findViewById(R.id.rss_list);
		adapter = new RSSListAdapter(getActivity());
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				Intent intent = new Intent(getActivity(), RSSItemDetailActivity.class);
				RSSListAdapter adapter = (RSSListAdapter) parent.getAdapter();
				RSSItem item = (RSSItem)adapter.getItem(position);
				intent.putExtra("url", item.getLink());
				getActivity().startActivityForResult(intent, 1);
				getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
	}
	
	private void setUpDialog() {
		dialog = ((RSSPagerActivity)getActivity()).getDialog();
	}
	
	public void loadItem() {
		task = new RSSAsyncTask(dialog);
		task.attachObserver(adapter);
		task.execute(rss_link);
	}
}
